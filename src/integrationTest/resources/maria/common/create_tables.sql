DROP DATABASE IF EXISTS pacific;
DROP DATABASE IF EXISTS franman;
DROP DATABASE IF EXISTS ctrade;


CREATE DATABASE franman;

USE franman;

CREATE TABLE franchises
(
    fr_code VARCHAR(6) PRIMARY KEY NOT NULL
);

CREATE DATABASE ctrade;

USE ctrade;

CREATE TABLE ct_suppliers
(
    sup_code VARCHAR(6) PRIMARY KEY NOT NULL
);

CREATE DATABASE pacific;

USE pacific;

CREATE TABLE order_headers
(
    id                     BIGINT(20) PRIMARY KEY              NOT NULL AUTO_INCREMENT,
    franchise_code         VARCHAR(6)                          NOT NULL,
    supplier_code          VARCHAR(6)                          NOT NULL,
    order_created          DATETIME                            NOT NULL,
    expected_delivery_date DATE,
    retailer_reference     VARCHAR(45),
    status                 VARCHAR(15)                         NOT NULL,
    is_special             VARCHAR(1) DEFAULT 'N'              NOT NULL,
    is_presell             VARCHAR(1),
    is_content             VARCHAR(1) DEFAULT 'N'              NOT NULL,
    year_number            SMALLINT(6)                         NOT NULL,
    week_number            TINYINT(4)                          NOT NULL,
    last_updated           TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    order_submitted        DATETIME,
    cut_off_timestamp      DATETIME,
    supplier_reference     VARCHAR(100),
    order_delivered        DATETIME,
    originating_system     VARCHAR(50),
    version                INT(11)                                      DEFAULT '0',
    CONSTRAINT order_headers_ibfk_1 FOREIGN KEY (franchise_code) REFERENCES franman.franchises (fr_code),
    CONSTRAINT order_headers_idfk_3 FOREIGN KEY (supplier_code) REFERENCES ctrade.ct_suppliers (sup_code)
);

CREATE TABLE order_lines
(
    id                           BIGINT(20) PRIMARY KEY              NOT NULL AUTO_INCREMENT,
    order_header_id              BIGINT(20)                          NOT NULL,
    deal_product_order_header_id BIGINT(20),
    standing_order_header_id     BIGINT(20),
    supplier_product_id          BIGINT(20),
    cost                         DECIMAL(9, 4),
    order_quantity               INT(11),
    product_code                 VARCHAR(20)                         NOT NULL,
    outer_barcode                VARCHAR(20),
    product_description          VARCHAR(255),
    case_size                    INT(11),
    unit_size                    DECIMAL(8, 3),
    unit_of_measure              VARCHAR(2),
    rsp                          DECIMAL(8, 4),
    invoice_cost                 DECIMAL(8, 4),
    distribution_cost            DECIMAL(8, 4),
    guaranteed_overrider         DECIMAL(8, 4),
    net_cost                     DECIMAL(8, 4),
    is_surcharge_applicable      VARCHAR(1),
    vat_rate                     DECIMAL(4, 2),
    tier_type                    VARCHAR(15),
    tier_quantity                INT(11),
    order_source                 VARCHAR(15)                         NOT NULL,
    is_composite                 VARCHAR(1) DEFAULT 'N'              NOT NULL,
    is_content                   VARCHAR(1) DEFAULT 'N'              NOT NULL,
    position                     INT(11)                             NOT NULL,
    flags                        SMALLINT(6)                                  DEFAULT '0'
    COMMENT 'Bit flags for status',
    last_modified                TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    original_product_code        VARCHAR(20),
    original_order_quantity      INT(11),
    substituted_line_id          BIGINT(20),
    pack_size                    INT(11),
    rejected_reason              VARCHAR(50),
    rejected_reason_string       VARCHAR(255),
    is_rsp_promotion             VARCHAR(1)                                   DEFAULT 'N',
    is_cost_promotion            VARCHAR(1)                                   DEFAULT 'N',
    inner_barcode                VARCHAR(20),
    super_category_code          VARCHAR(6),
    category_code                VARCHAR(6),
    sub_category_code            VARCHAR(6),
    sub_sub_category_code        VARCHAR(6),
    sku_id                       BIGINT(20),
    ni_core_range                VARCHAR(1),
    mainland_core_range          VARCHAR(1),
    subsidiary_kicker_percentage DECIMAL(6, 4),
    generic_sku_description      VARCHAR(100),
    promo_bonus                  DECIMAL(8, 4),
    substitution_strategy        VARCHAR(50)
);

CREATE TABLE `order_lines_content` (
    `order_line_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`order_line_id`),
    CONSTRAINT `fk_order_lines` FOREIGN KEY (`order_line_id`) REFERENCES `order_lines` (`id`)
);

CREATE TABLE `order_lines_flags` (
    `order_line_id` BIGINT(20) NOT NULL,
    `omp_oos`       VARCHAR(1) NOT NULL DEFAULT 'N',
    `upsold_from`   VARCHAR(255),
    PRIMARY KEY (`order_line_id`),
    CONSTRAINT `fk_order_lines_flags_order_line_id` FOREIGN KEY (`order_line_id`) REFERENCES `order_lines` (`id`)
);

CREATE TABLE product_information (
    supplier_code                   VARCHAR(6)    NOT NULL,
    supplier_product_code           VARCHAR(20)   NOT NULL,
    effective_date                  DATE          NOT NULL,
    end_date                        DATE                   DEFAULT NULL,
    gram_weight                     INT(11)                DEFAULT NULL,
    inner_count                     INT(11)       NOT NULL,
    long_count_outer                VARCHAR(1)             DEFAULT NULL,
    outer_mm_depth                  INT(11)                DEFAULT NULL,
    outer_mm_height                 INT(11)                DEFAULT NULL,
    outer_mm_width                  INT(11)                DEFAULT NULL,
    money_off_outer                 VARCHAR(1)             DEFAULT NULL,
    range_status                    VARCHAR(12)   NOT NULL,
    short_count_outer               VARCHAR(1)             DEFAULT NULL,
    consumer_added_value            VARCHAR(1)             DEFAULT NULL,
    consumer_offer                  VARCHAR(1)             DEFAULT NULL,
    core_range                      VARCHAR(1)             DEFAULT NULL,
    used_in_surcharge_calculation   VARCHAR(1)             DEFAULT NULL,
    used_in_surcharge_qualification VARCHAR(1)             DEFAULT NULL,
    money_off_consumer_unit         VARCHAR(1)             DEFAULT NULL,
    non_barcoded_consumer_unit      VARCHAR(1)             DEFAULT NULL,
    original_supplier_product_code  VARCHAR(45)            DEFAULT NULL,
    inner_barcode                   VARCHAR(20)            DEFAULT NULL,
    outer_barcode                   VARCHAR(20)            DEFAULT NULL,
    outer_outer_barcode             VARCHAR(20)            DEFAULT NULL,
    price_checked                   VARCHAR(1)             DEFAULT NULL,
    product_description             VARCHAR(255)           DEFAULT NULL,
    short_product_description       VARCHAR(255)           DEFAULT NULL,
    abv_percentage                  DECIMAL(3, 1)          DEFAULT NULL,
    brand                           VARCHAR(45)            DEFAULT NULL,
    inner_mm_depth                  INT(11)                DEFAULT NULL,
    inner_mm_height                 INT(11)                DEFAULT NULL,
    inner_mm_width                  INT(11)                DEFAULT NULL,
    original_supplier               VARCHAR(100)           DEFAULT NULL,
    own_label                       VARCHAR(1)             DEFAULT NULL,
    pack_size                       INT(11)                DEFAULT NULL,
    shelf_life                      INT(11)                DEFAULT NULL,
    unit_of_measure                 VARCHAR(2)    NOT NULL,
    unit_size                       DECIMAL(8, 3) NOT NULL,
    units_of_alcohol                DECIMAL(3, 1)          DEFAULT NULL,
    vat_rate                        DECIMAL(4, 2) NOT NULL,
    cost_to_serve                   DECIMAL(9, 4)          DEFAULT NULL,
    invoice_cost                    DECIMAL(9, 4)          DEFAULT NULL,
    cost_to_site                    DECIMAL(9, 4)          DEFAULT NULL,
    rsp                             DECIMAL(7, 2)          DEFAULT NULL,
    cost_band_description           VARCHAR(255)           DEFAULT NULL,
    rsp_band_description            VARCHAR(255)           DEFAULT NULL,
    is_cost_promotion               VARCHAR(1)             DEFAULT 'N',
    category_id                     BIGINT(20)    NOT NULL,
    category_description            VARCHAR(100),
    is_rsp_promotion                VARCHAR(1)             DEFAULT 'N',
    guaranteed_overrider            DECIMAL(9, 4)          DEFAULT NULL,
    supplier_product_id             BIGINT(20)    NOT NULL,
    cost_band_id                    BIGINT(20)             DEFAULT NULL,
    rsp_band_id                     BIGINT(20)             DEFAULT NULL,
    id                              BIGINT(20)    NOT NULL AUTO_INCREMENT,
    net_cost                        DECIMAL(9, 4)          DEFAULT NULL,
    distribution_cost               DECIMAL(9, 4)          DEFAULT NULL,
    last_modified                   TIMESTAMP     NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp,
    unit_sort                       BIGINT(20)             DEFAULT NULL,
    super_category_code             VARCHAR(6)             DEFAULT NULL,
    category_code                   VARCHAR(6)             DEFAULT NULL,
    sub_category_code               VARCHAR(6)             DEFAULT NULL,
    sub_sub_category_code           VARCHAR(6)             DEFAULT NULL,
    margin                          DECIMAL(5, 2)          DEFAULT NULL,
    sku_id                          BIGINT(20)             DEFAULT NULL,
    ni_core_range                   VARCHAR(1)    NOT NULL DEFAULT 'N',
    mainland_core_range             VARCHAR(1)    NOT NULL DEFAULT 'N',
    subsidiary_kicker_percentage    DECIMAL(6, 4)          DEFAULT NULL,
    generic_sku_description         VARCHAR(100)           DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY idx_unique_constraint (supplier_product_id, effective_date, cost_band_id, rsp_band_id)
);

CREATE TABLE order_delivery_intersect
(
    order_header_id    BIGINT(20) NOT NULL,
    delivery_header_id BIGINT(20) NOT NULL,
    CONSTRAINT `PRIMARY` PRIMARY KEY (order_header_id, delivery_header_id)
);

CREATE TABLE delivery_headers
(
    id                  BIGINT(20) PRIMARY KEY              NOT NULL,
    delivery_date       DATE                                NOT NULL,
    delivery_reference  VARCHAR(45),
    order_id            INT(11),
    invoice_number      VARCHAR(17),
    depot_code          VARCHAR(1),
    file_name           VARCHAR(20),
    franchise_code      VARCHAR(6),
    supplier_code       VARCHAR(6)                          NOT NULL,
    invoice_transmitted TIMESTAMP,
    total_cases         INT(11),
    total_cost          DECIMAL(9, 2),
    total_vat           DECIMAL(9, 2),
    total_value         DECIMAL(9, 2)
);

CREATE TABLE delivery_lines
(
    id                    BIGINT(20) PRIMARY KEY              NOT NULL,
    delivery_header_id    BIGINT(20)                          NOT NULL,
    order_line_id         BIGINT(20),
    delivery_quantity     INT(11)                             NOT NULL,
    product_code          VARCHAR(20),
    outer_barcode         VARCHAR(20),
    inner_barcode         VARCHAR(20),
    case_size             INT(11),
    product_description   VARCHAR(255)                        NOT NULL,
    position              INT(11)                             NOT NULL,
    rsp                   DECIMAL(9, 2),
    invoice_cost          DECIMAL(8, 4),
    distribution_cost     DECIMAL(8, 4),
    guaranteed_overrider  DECIMAL(8, 4),
    net_cost              DECIMAL(8, 4),
    cost                  DECIMAL(9, 2),
    vat_rate              VARCHAR(1),
    total_cost            DECIMAL(9, 2),
    original_product_code VARCHAR(20),
    parent_product_code   VARCHAR(20),
    free_stock            VARCHAR(1),
    product_weight        VARCHAR(20),
    product_type          INT(11),
    price_marked          VARCHAR(1)
);

CREATE TABLE supplier_product_range_restrictions (
    supplier_product_id BIGINT(20)  NOT NULL,
    sd_code             VARCHAR(30) NOT NULL,
    rc_code             VARCHAR(20) NOT NULL,
    PRIMARY KEY (supplier_product_id, rc_code, sd_code)
);

CREATE TABLE `submission_headers` (
    `id`                     BIGINT(20)    NOT NULL AUTO_INCREMENT,
    `supplier_account_code`  VARCHAR(20)   NULL     DEFAULT NULL,
    `franchise_code`         VARCHAR(8)    NULL     DEFAULT NULL,
    `supplier_code`          VARCHAR(10)   NOT NULL,
    `bad_order_id`           VARCHAR(16)   NULL     DEFAULT NULL,
    `delivery_note_number`   VARCHAR(10)   NULL     DEFAULT NULL,
    `expected_delivery_date` DATE          NULL     DEFAULT NULL,
    `invoice_date`           DATE          NULL     DEFAULT NULL,
    `transaction_type`       CHAR(1)       NULL     DEFAULT NULL,
    `total_cases`            INT(11)       NULL     DEFAULT NULL,
    `total_cost`             DECIMAL(9, 2) NULL     DEFAULT NULL,
    `total_vat`              DECIMAL(9, 2) NULL     DEFAULT NULL,
    `file_name`              VARCHAR(20)   NULL     DEFAULT NULL,
    `order_date`             DATE          NULL     DEFAULT NULL,
    `total_value`            DECIMAL(9, 2) NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `submission_lines` (
    `id`                    BIGINT(20)    NOT NULL AUTO_INCREMENT,
    `submission_header_id`  BIGINT(20)    NOT NULL,
    `line_number`           INT(11)       NOT NULL,
    `order_line_id`         BIGINT(20)    NULL     DEFAULT NULL,
    `product_code`          VARCHAR(20)   NULL     DEFAULT NULL,
    `inner_bar_code`        VARCHAR(13)   NULL     DEFAULT NULL,
    `product_description`   VARCHAR(255)  NULL     DEFAULT NULL,
    `unit_size_measure`     VARCHAR(6)    NULL     DEFAULT NULL,
    `rrp`                   DECIMAL(6, 2) NULL     DEFAULT NULL,
    `invoice_cost`          DECIMAL(8, 4) NULL     DEFAULT NULL,
    `distribution_cost`     DECIMAL(8, 4) NULL     DEFAULT NULL,
    `guaranteed_overrider`  DECIMAL(8, 4) NULL     DEFAULT NULL,
    `net_cost`              DECIMAL(8, 4) NULL     DEFAULT NULL,
    `case_size`             DECIMAL(4, 0) NULL     DEFAULT NULL,
    `last_rrp_change`       DATE          NULL     DEFAULT NULL,
    `cases_ordered`         DECIMAL(7, 2) NULL     DEFAULT NULL,
    `cases_accepted`        DECIMAL(7, 2) NULL     DEFAULT NULL,
    `pack_cost`             DECIMAL(8, 3) NULL     DEFAULT NULL,
    `net_line_value`        DECIMAL(7, 2) NULL     DEFAULT NULL,
    `vat_percentage`        DECIMAL(4, 2) NULL     DEFAULT NULL,
    `group_code`            VARCHAR(9)    NULL     DEFAULT NULL,
    `vat_code`              CHAR(1)       NULL     DEFAULT NULL,
    `weight`                DECIMAL(7, 3) NULL     DEFAULT NULL,
    `outer_bar_code`        VARCHAR(14)   NULL     DEFAULT NULL,
    `original_product_code` VARCHAR(13)   NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `FK_SUB_HEADER`     (`submission_header_id`),
    INDEX `FK_SUB_LINE_LINE`  (`order_line_id`),
    CONSTRAINT `FK_SUB_HEADER` FOREIGN KEY (`submission_header_id`) REFERENCES `submission_headers` (`id`),
    CONSTRAINT `FK_SUB_LINE_LINE` FOREIGN KEY (`order_line_id`) REFERENCES `order_lines` (`id`)
);


DROP DATABASE IF EXISTS system_domains;

CREATE DATABASE system_domains;

USE system_domains;

CREATE TABLE system_domains (
    id   BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(64)         NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE reference_codes (
    id    BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    sd_id BIGINT(20) UNSIGNED NOT NULL,
    name  VARCHAR(64)         NOT NULL,
    PRIMARY KEY (`id`)
);