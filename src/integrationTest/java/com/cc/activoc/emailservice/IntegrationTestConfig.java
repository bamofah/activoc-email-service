package com.cc.activoc.emailservice;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.MariaDB4jService;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.cc.activoc.emailservice.config.AppConfig;
import com.cc.rest.config.AbstractWebMvcConfigurer;
import com.cc.rest.exception.ExceptionControllerAdvice;
import com.cc.rest.exception.ExceptionRestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.openqa.selenium.lift.find.ImageFinder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.beans.PropertyVetoException;
import java.time.Clock;
import java.time.LocalDate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com.cc.activoc.emailservice",
    excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = {AppConfig.class}))
public class IntegrationTestConfig extends AbstractWebMvcConfigurer {

    @Value("${dynamicPorts.MARIA_DB_PORT:24589}")
    private int dbPort;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new
            PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);

        return propertySourcesPlaceholderConfigurer;
    }

    @Bean(destroyMethod = "stop")
    public MariaDB4jService mariaDB4jService() {
        MariaDB4jService mariaDB4jService = new MariaDB4jService();
        mariaDB4jService.getConfiguration()
                        .setBaseDir("build/tmp/maria/bin")
                        .setDataDir("build/tmp/maria/data")
                        .setPort(dbPort);

        return mariaDB4jService;
    }

    @Bean
    public DataSource dataSource(MariaDB4jService mariaDB4jService) throws ManagedProcessException,
        PropertyVetoException {
        DB db = mariaDB4jService.getDB();
        db.source("maria/common/create_tables.sql");
        db.source("maria/common/insert_system_domains.sql");

        DataSource dataSource = new DataSource();
        dataSource.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
        dataSource.setUrl("jdbc:mysql://localhost:" + dbPort + "/pacific");
        dataSource.setUsername("sa");
        dataSource.setPassword("");

        return dataSource;
    }

    @Bean
    public RestTemplate signedRestTemplate() throws Exception {
        RestTemplate restTemplate = ExceptionRestTemplate.getRestTemplate("integration-test");
        updateRestTemplate(restTemplate);
        return restTemplate;
    }

    @Bean
    public RestTemplate signedLegacyRestTemplate() throws Exception {
        return signedRestTemplate();
    }

    @Bean
    public ExceptionControllerAdvice exceptionControllerAdvice() {
        return new ExceptionControllerAdvice();
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public ImageFinder defaultImageFinder() {
        return mock(ImageFinder.class);
    }


    @Bean
    public ObjectMapper objectMapper() {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JSR310Module());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return objectMapper;
    }

    @Bean
    public Clock clock() {
        // Dummy clock object to manipulate as required in tests
        Clock mockClock = mock(Clock.class);

        Clock defaultClock = Clock.systemDefaultZone();
        when(mockClock.instant()).thenReturn(LocalDate.of(2017, 1, 1).atStartOfDay(defaultClock.getZone()).toInstant());
        when(mockClock.getZone()).thenReturn(defaultClock.getZone());
        return mockClock;
    }


    @Bean
    public AmazonSimpleEmailService setUpAmazoneEmailService() {

        AmazonSimpleEmailService simpleEmailService = AmazonSimpleEmailServiceClientBuilder
            .standard()
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .withRegion(Regions.EU_WEST_1)
            .build();

        return simpleEmailService;
    }

}
