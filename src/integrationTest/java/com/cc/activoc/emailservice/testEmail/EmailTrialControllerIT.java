package com.cc.activoc.emailservice.testEmail;

import com.cc.activoc.emailservice.IntegrationTest;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EmailTrialControllerIT extends IntegrationTest {

    @Test
    @Issues("AOC-1563")
    @Title("should return 200 when email sent")
    public void testShouldReturn200EmailSuccessfull() throws Exception {

        mockMvc.perform(post("/email"))
               .andDo(print())
               .andExpect(status().isOk());
    }

}