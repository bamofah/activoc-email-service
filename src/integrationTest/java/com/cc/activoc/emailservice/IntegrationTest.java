package com.cc.activoc.emailservice;

import net.serenitybdd.junit.spring.integration.SpringIntegrationSerenityRunner;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
@RunWith(SpringIntegrationSerenityRunner.class)
@ContextConfiguration(classes = IntegrationTestConfig.class)
@TestPropertySource(properties = {
    "activoc-email-service.ses.region = eu-west-1"
})

public abstract class IntegrationTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    @Qualifier("signedRestTemplate")
    private RestTemplate restTemplate;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;
    protected MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(webApplicationContext)
            .build();

        mockServer = MockRestServiceServer.createServer(restTemplate);
    }
}
