package com.cc.activoc.emailservice.testEmail;

import com.cc.rest.server.SignedRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailTrialController {

    @Autowired
    private EmailServiceTrial emailServiceTrial;


    @SignedRequest
    @ApiOperation("Send Test Email")
    @PostMapping("/email")
    public void email() {

        emailServiceTrial.sendTestMail();
    }
}
