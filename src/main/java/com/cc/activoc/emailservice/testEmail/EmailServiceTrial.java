package com.cc.activoc.emailservice.testEmail;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class EmailServiceTrial {

    Logger LOG = LoggerFactory.getLogger(EmailServiceTrial.class);


    @Autowired
    private AmazonSimpleEmailService amazonSimpleEmailService;

    public void sendTestMail() {

        try {

            SendEmailRequest request = new SendEmailRequest();
            Destination destination = new Destination();
            Content subjectContent = new Content();
            Content bodyContent = new Content();
            Body messageBody = new Body();

            subjectContent.withData("Test Email using AmazoneSimpleEmailServie").withCharset("UTF-8");

            bodyContent.withData("Hello From Me").setCharset("UTF-8");


            messageBody.setHtml(bodyContent);
            Message message = new Message(subjectContent, messageBody);


            destination.withToAddresses("bernard.amofah@costcutter.com");

            request.withSource("bernard.amofah@costcutter.com");
            request.withDestination(destination);
            request.withMessage(message);

            SendEmailResult emailResult = amazonSimpleEmailService.sendEmail(request);
             LOG.debug("Message with id: {} successfully sent", emailResult.getMessageId());

        } catch (AmazonClientException ex) {
            LOG.debug("Error throwing whilst sending email {}", ex.getMessage());

            throw new AmazonClientException("Error Sending email");
        }


    }


}
