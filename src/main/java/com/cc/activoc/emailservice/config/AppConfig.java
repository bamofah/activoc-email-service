package com.cc.activoc.emailservice.config;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.cc.rest.KeyStoreFactory;
import com.cc.rest.RequestHeaders;
import com.cc.rest.client.SignedRestTemplate;
import com.cc.rest.config.AbstractWebMvcConfigurer;
import com.cc.rest.exception.ExceptionControllerAdvice;
import com.cc.rest.handler.SignedHandler;
import com.mysql.jdbc.Driver;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.KeyStore;
import java.sql.Date;
import java.time.Clock;
import java.time.LocalDate;
import java.util.Locale;

@EnableWebMvc
@Configuration
@EnableSwagger2
@EnableTransactionManagement
@ComponentScan(basePackages = "com.cc.activoc.emailservice")
@PropertySource(ignoreResourceNotFound = true,
    value = {
        "${properties.path}/activoc-email-service.properties",
        "${properties.path}/activoc-email-service-private.properties"
    }
)
public class AppConfig extends AbstractWebMvcConfigurer {

    @Value("${appName}")
    private String appName;

    @Value("${db.activoc.rw.url}")
    private String dbUrl;

    @Value("${db.activoc.rw.username}")
    private String dbUsername;

    @Value("${db.activoc.rw.password}")
    private String dbPassword;

    @Value("${db.activoc.rw.initialSize}")
    private int dbInitialSize;

    @Value("${db.activoc.rw.maxActive}")
    private int dbMaxActive;

    @Value("${db.activoc.rw.testOnBorrow}")
    private boolean dbTestOnBorrow;

    @Value("${db.activoc.rw.testWhileIdle}")
    private boolean dbTestWhileIdle;

    @Value("${db.activoc.rw.validationQuery}")
    private String dbValidationQuery;

    @Value("${db.activoc.rw.validationQueryTimeout}")
    private int dbValidationQueryTimeout;

    @Value("${db.activoc.rw.validationInterval}")
    private long dbValidationInterval;

    @Value("${db.activoc.rw.jdbcInterceptors}")
    private String dbJdbcInterceptors;

    @Value("${db.activoc.rw.logAbandoned}")
    private boolean dbLogAbandoned;

    @Value("${db.activoc.rw.logValidationErrors}")
    private boolean dbLogValidationErrors;

    @Value("${db.activoc.rw.maxWait}")
    private int dbMaxWait;

    @Value("${db.activoc.rw.propagateInterruptState}")
    private boolean dbPropagateInterruptState;

    @Value("${db.activoc.rw.removeAbandoned}")
    private boolean dbRemoveAbandoned;

    @Value("${db.activoc.rw.removeAbandonedTimeout}")
    private int dbRemoveAbandonedTimeout;

    @Value("${activoc-email-service.keystore.password}")
    private String keystorePassword;

    @Value("${activoc-email-service.keystore.private.alias}")
    private String keystorePrivateAlias;

    @Value("${activoc-email-service.keystore.private.password}")
    private String keystorePrivatePassword;

    @Value("${activoc-email-service.ses.config.defaultemail}")
    private String defaultEmail;

    @Value("${activoc-email-service.ses.region}")
    private String AWS_REGION;


    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ExceptionControllerAdvice exceptionControllerAdvice() {
        return new ExceptionControllerAdvice();
    }

    @Bean
    public KeyStore keyStore() throws Exception {
        return KeyStoreFactory.getKeyStore(appName, keystorePassword);
    }

    @Bean
    public RestTemplate signedRestTemplate(KeyStore keyStore) throws Exception {
        SignedRestTemplate restTemplate = new SignedRestTemplate(keyStore, appName, keystorePrivatePassword);
        updateRestTemplate(restTemplate);
        return restTemplate;
    }

    @Bean
    public SignedHandler signedHandler(KeyStore keyStore) throws Exception {
        return new SignedHandler(keyStore);
    }

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = new DataSource();
        dataSource.setDriverClassName(Driver.class.getName());
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        dataSource.setInitialSize(dbInitialSize);
        dataSource.setMaxActive(dbMaxActive);
        dataSource.setTestOnBorrow(dbTestOnBorrow);
        dataSource.setTestWhileIdle(dbTestWhileIdle);
        dataSource.setValidationQuery(dbValidationQuery);
        dataSource.setValidationQueryTimeout(dbValidationQueryTimeout);
        dataSource.setValidationInterval(dbValidationInterval);
        dataSource.setJdbcInterceptors(dbJdbcInterceptors);
        dataSource.setLogAbandoned(dbLogAbandoned);
        dataSource.setLogValidationErrors(dbLogValidationErrors);
        dataSource.setMaxWait(dbMaxWait);
        dataSource.setPropagateInterruptState(dbPropagateInterruptState);
        dataSource.setRemoveAbandoned(dbRemoveAbandoned);
        dataSource.setRemoveAbandonedTimeout(dbRemoveAbandonedTimeout);
        dataSource.setDefaultReadOnly(Boolean.FALSE);

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod
                    .DELETE.name())
                .allowedHeaders(HttpHeaders.AUTHORIZATION, HttpHeaders.CONTENT_TYPE, RequestHeaders.FRANCHISE_CODE);
    }

    @Bean
    public Docket docket() {
        ApiInfo apiInfo = new ApiInfoBuilder()
            .title(appName)
            .build();

        return new Docket(DocumentationType.SWAGGER_2)
            .directModelSubstitute(LocalDate.class, Date.class)
            .apiInfo(apiInfo);
    }

    @Bean
    public AmazonSimpleEmailService setUpAmazoneEmailService() {

        AmazonSimpleEmailService simpleEmailService = AmazonSimpleEmailServiceClientBuilder
            .standard()
            .withCredentials(
                new DefaultAWSCredentialsProviderChain())
            .withRegion(AWS_REGION.trim().toLowerCase(Locale.ENGLISH))
            .build();

        return simpleEmailService;
    }

    @Bean
    public Clock clock() {
        // Inject clock to where required.  This is to allow
        // an alternative clock to be injected when testing
        return Clock.systemDefaultZone();
    }

}
