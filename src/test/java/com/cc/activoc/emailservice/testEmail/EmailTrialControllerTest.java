package com.cc.activoc.emailservice.testEmail;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.cc.activoc.emailservice.utils.TestSecurityUtils.assertAllEndpointsAreSecured;

@RunWith(SerenityRunner.class)
public class EmailTrialControllerTest {


    @Test
    @Issue("AOC-1563")
    @Title("RSA authentication check ")
    public void shouldSecureAllEnpointsWithRSASecurity() {

        assertAllEndpointsAreSecured(EmailTrialController.class);
    }
}