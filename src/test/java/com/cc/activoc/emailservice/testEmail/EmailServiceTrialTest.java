package com.cc.activoc.emailservice.testEmail;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Title;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.validateMockitoUsage;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SerenityRunner.class)
public class EmailServiceTrialTest {

    @InjectMocks
    private EmailServiceTrial emailServiceTrial;

    @Mock
    private AmazonSimpleEmailService simpleEmailService;

    @Captor
    private ArgumentCaptor<SendEmailRequest> requestArgumentCaptor;


    @Before
    public void setUp() {
        initMocks(this);

    }

    @After
    public void tearDown() {
        validateMockitoUsage();
    }

    @Test
    @Issue("AOC-1563")
    @Title("Send Email Test")
    public void testSendEmail() {

        when(simpleEmailService.sendEmail(requestArgumentCaptor.capture()))
            .thenReturn(new SendEmailResult().withMessageId("123"));

        emailServiceTrial.sendTestMail();
        verify(simpleEmailService).sendEmail(requestArgumentCaptor.capture());
        SendEmailRequest ask = requestArgumentCaptor.getValue();

        assertEquals("Should have correct subject","Test Email using AmazoneSimpleEmailServie", ask.getMessage().getSubject().getData());
        assertEquals("Should have correct message", "Hello From Me", ask.getMessage().getBody().getHtml().getData());
        assertEquals("Should have correct toAddress", "bernard.amofah@costcutter.com", ask.getDestination().getToAddresses().get(0));

    }

    @Test(expected = AmazonClientException.class)
    @Issue("AOC-1563")
    @Title("Should throw an exception")
    public void shouldThrowAmazonClientException() throws Exception {


        when(simpleEmailService.sendEmail(isA(SendEmailRequest.class)))
            .thenThrow(new AmazonClientException("Error"));

        emailServiceTrial.sendTestMail();
    }

}