package com.cc.activoc.emailservice.utils;

import com.cc.rest.oauth.OAuthSecured;
import com.cc.rest.server.SignedRequest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class TestSecurityUtils {

    private TestSecurityUtils() {
        //prevent construction
    }

    public static void assertAllEndpointsAreSecured(Class<?> cls) {

        Method[] declaredMethods = cls.getDeclaredMethods();

        assertTrue("No declared methods in class", declaredMethods.length > 0);

        for (Method method : declaredMethods) {
            if (Modifier.isPublic(method.getModifiers())) {
                Annotation[] declaredAnnotations = method.getDeclaredAnnotations();

                assertTrue("Method '" + method.getName() + "' is not annotated with @SignedRequest",
                    annotatedWithSignedRequest(declaredAnnotations));
                assertFalse("Method '" + method.getName() + "' is annotated with @OAuthSecured",
                    annotatedWithOAuthSecured(declaredAnnotations));
            }
        }
    }

    private static boolean annotatedWithSignedRequest(Annotation... annotations) {
        return annotatedWith(SignedRequest.class, annotations);
    }

    private static boolean annotatedWithOAuthSecured(Annotation... annotations) {
        return annotatedWith(OAuthSecured.class, annotations);
    }

    private static boolean annotatedWith(Class<?> annotationClass, Annotation... annotations) {
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> cls = annotation.annotationType();
            if (cls.isAssignableFrom(annotationClass)) {
                return true;
            }
        }

        return false;
    }
}
